from flask import Flask
from flask import send_from_directory
from flask import request
from flask_cors import CORS
import mysql.connector

app = Flask(__name__)
CORS(app)

@app.route('/login', methods=['GET', 'POST'])
def parse_request():
    print(request.form.get('name'))
    mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      password="test",
      database="webdb"
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO login (name, password) VALUES (%s, %s)"
    val = (request.form.get('name'), request.form.get('password'))
    mycursor.execute(sql, val)
    mydb.commit()
    return "<p>登录成功</p>"
@app.route("/hi")
def hello_world():
    return "<p>Hello, World!</p>"
@app.route('/web/<path:path>')
def static_file(path):
    return send_from_directory('web', path)

